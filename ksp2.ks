run util.ks.
run orbit.ks.

set targetAltitude to 75000.
sas off.
rcs off.

Clearscreen.
Stage.
Escape(targetAltitude,3).
Clearscreen.
CoastTo(40000).
FineTuneApoapsis(targetAltitude).
Clearscreen.

print "Waiting to crash...".
lock steering to ship:prograde.
until ship:isdead.
