
function RecordAllScience {
	SET totalScience TO 0.
    FOR m IN ship:modulesnamed("ModuleScienceExperiment") {
    	IF M:HASDATA {
    		M:RESET().
    	}
	    M:DEPLOY.
	    WAIT UNTIL M:HASDATA.
	    SET thisScience TO 0.
	    FOR data IN M:DATA {
	  		IF (data:transmitvalue>0) {
	        	PRINT "Recorded [ "+m:part:name+" ] for [ "+ROUND(data:transmitvalue,9)+" ] science".
	        	SET thisScience TO data:transmitvalue+thisScience.
	    	}
	    }
	   	IF thisScience=0 {
	   		M:RESET().
	   	}
	   	SET totalScience TO totalScience+thisScience.
    }
    IF totalScience = 0 {
    	PRINT "No new science recorded.".
    }
}.

function TransmitAllScience {
    FOR m IN ship:modulesnamed("ModuleScienceExperiment") {
	    IF M:HASDATA {
		    SET thisScience TO 0.
		    FOR data IN M:DATA {
		    	IF (data:transmitvalue>0) {
		        	PRINT "Transmitting [ "+m:part:name+" ] for [ "+ROUND(data:transmitvalue,9)+" ] science".
		        	SET thisScience TO data:transmitvalue+thisScience.
		    	}
		    }
		   	IF thisScience=0 {
		   		M:RESET().
		   	} ELSE {
		   		M:TRANSMIT().
		   	}
	   	}
	}
	wait until OneAntenna("Uploading Data...").
	WaitForAllAntenna("Idle").
}

function WaitForAllAntenna {
	parameter state to "Idle".
	for m in ship:modulesnamed("ModuleDataTransmitter") {
		//print "antenna [ "+m:name+ " ] from [ "+m:part:name+" ] is [ "+m:getfield("antenna state") +" ]".
		wait until m:getfield("antenna state") = state.
	}
}

function OneAntenna {
	parameter state to "Uploading Data...".
	for m in ship:modulesnamed("ModuleDataTransmitter") {
		if m:getfield("antenna state") = state {
			print "antenna [ "+m:name+ " ] from [ "+m:part:name+" ] is [ "+m:getfield("antenna state") +" ]".
			return true.
		}
	}
	return false.
}


RecordAllScience().
TransmitAllScience().