CORE:PART:GETMODULE("kOSProcessor"):DOEVENT("Open Terminal").

FUNCTION Format {
	PARAMETER number.
	
	SET str TO ""+number.
	SET index TO str:FIND(".").
	IF(index >= 0){
		SET length TO index+3.
		IF(length>str:length) {
			SET length TO str:length.
		}
		SET str TO str:SUBSTRING(0,length).
	}
	RETURN str.
}

FUNCTION RightAlign {
	PARAMETER s.
	PARAMETER length.
	IF(length<s:length) {
		return s.
	}
	return Repeat(length-s:length," ")+s.
}

FUNCTION Repeat {
	PARAMETER length.
	PARAMETER s.
	
	SET str TO "".
	
	FROM { local i is 0. }
	UNTIL i = length-1
	STEP { SET i to i+1. }
	DO { SET str TO str+s. }
	
	RETURN str.
}

FUNCTION Underline {
	PARAMETER length.
	RETURN Repeat(length,"-").
}

FUNCTION DrawScreen {
	PARAMETER screen.
	PARAMETER x IS 0.
	PARAMETER y IS 0.
	PARAMETER labelLength IS 20.
	PARAMETER width IS Terminal:Width/2.
	
	// Screen Title
	PRINT screen["name"] AT(x,y).
	PRINT Underline(width) AT(x,y+1).
	
	// Get Fields
	FROM {
		local i is 0.
		local itr is screen["fields"]:iterator.
	}
	UNTIL not itr:next
	STEP { SET i to i+1. }
	DO {
		if(NOT (itr:value = "")) {
			PRINT itr:value AT (x+1,y+i+2).
			PRINT RightAlign(Format(screen[itr:value]),width-labelLength-2) AT (x+labelLength+1,y+i+2).
		}
	}
}

FUNCTION CreateBase {
	PARAMETER name.
		
	FUNCTION CommonScreen {
		LOCAL screen TO lexicon().
		screen:add("name","Details").
		screen:add("fields",list("Flight Mode","","Apoapsis","Periapsis","","Altitude")).
		screen:add("Flight Mode",name).
		screen:add("Apoapsis",apoapsis).
		screen:add("Periapsis",periapsis).
		
		screen:add("Altitude",altitude).
		return screen.
	}
	
	LOCAL lex TO lexicon().
	lex:add("name",name).
	lex:add("run", {
		CLEARSCREEN.
		lex["main"]().
	}).
	lex:add("next", {
		IF(lex:hasKey("after")){
			lex["after"]["run"]().
		}
	}).
	lex:add("draw", {
		IF(lex:hasKey("screen")){
			DrawScreen(lex["screen"]()).
		}
		DrawScreen(CommonScreen(),Terminal:Width/2).
	}).
	return lex.
}

FUNCTION CreateCountDown {
	PARAMETER count IS 10.
	LOCAL countdown IS count.

	LOCAL lex TO CreateBase("countdown").
	
	lex:add("screen", {
		LOCAL screen TO lexicon().
		screen:add("name","Count Down").
		screen:add("fields",list("Count")).
		screen:add("Count",countdown).
		return screen.
	}).
	
	lex:add("main",{
		PRINT "CountDown.".
		FROM { }
		UNTIL countdown = 0
		STEP { SET countdown to countdown - 1. }
		DO {
			lex["draw"]().
			WAIT 1.
		}
		lex["next"]().
	}).
	return lex.
}

FUNCTION CreateStart {
	PARAMETER targetspeed IS 100.
	LOCAL lex TO CreateBase("start").
	
	lex:add("screen", {
		LOCAL screen TO lexicon().
		screen:add("name","Take Off").
		screen:add("fields",list("Ground Speed","Target")).
		screen:add("Ground Speed",groundspeed).
		screen:add("Target",targetspeed).		
		return screen.
	}).
	
	lex:add("main",{
		TOGGLE AG1.
		BRAKES OFF.
		LOCK throttle TO 1.0.
		LOCK steering TO heading(90,0).
		UNTIL groundspeed > targetspeed {
			lex["draw"]().
		}
		UNLOCK throttle.
		UNLOCK steering.
		lex["next"]().
	}).
	return lex.
}

FUNCTION CreateLiftOff {
	PARAMETER targetspeed IS 400.
	PARAMETER pitchUpInitial IS 30.
	PARAMETER pitchUpFinal IS 10.
	
	LOCAL sset TO heading(0,0).
	
	LOCAL lex TO CreateBase("liftoff").
	
	lex:add("screen", {
		LOCAL screen TO lexicon().
		screen:add("name","Take Off").
		screen:add("fields",list("Air Speed","Target","","Gear","","Pitch Initial","Pitch Final")).
		screen:add("Air Speed",airspeed).
		screen:add("Target",targetspeed).
		screen:add("Gear",GEAR).
		screen:add("Pitch Initial",pitchUpInitial).
		screen:add("Pitch Final",pitchUpFinal).
		return screen.
	}).
	
	lex:add("main",{
		LOCK throttle TO 1.0.
		LOCK steering TO sset.
		
		SET sset TO heading(90,pitchUpInitial).
		UNTIL verticalspeed > 5 {
			lex["draw"]().
			SET sset TO heading(90,pitchUpInitial).
		}
		GEAR OFF.
		
		SET sset TO heading(90,pitchUpFinal).
		UNTIL airspeed > targetspeed {
			lex["draw"]().
			SET sset TO heading(90,pitchUpFinal).
		}
		
		UNLOCK throttle.
		UNLOCK steering.
		
		lex["next"]().
	}).
	return lex.
}

FUNCTION CreateCruise {
	PARAMETER targetaltitude IS 20000.
	PARAMETER targetspeed IS 1600.
	PARAMETER pitchMax IS 25.
	PARAMETER pitchMin IS 5.
	PARAMETER pitchTime IS 100.
	PARAMETER pitchSensitivity TO 0.5.
	
	LOCAL maxPitchChange TO 2.
	LOCAL altitudedifference TO 0.
	LOCAL timetoaltitude TO 0.
	LOCAL predictedairspeed TO 0.
	LOCAL currentPitch TO 0.
	
	LOCAL sset TO heading(0,0).
	
	LOCAL lex TO CreateBase("cruise").
	
	lex:add("screen", {
		LOCAL screen TO lexicon().
		screen:add("name","Cruise").
		screen:add("fields",list(
			"Altitude","Altitude Target","Altitude Difference", "",
			"Time To Altitude","",
			"Airspeed","Airspeed Target","Airspeed Prediction","",
			"Current Pitch")).
		screen:add("Airspeed",airspeed).
		screen:add("Airspeed Target",targetspeed).
		screen:add("Airspeed Prediction",predictedairspeed).
		screen:add("Altitude",altitude).
		screen:add("Altitude Target",targetaltitude).
		screen:add("Altitude Difference",altitudedifference).
		screen:add("Time To Altitude",timetoaltitude).
		screen:add("Current Pitch",currentPitch).
		return screen.
	}).
	
	lex:add("main",{
		LOCK throttle TO 1.0.
		LOCK steering TO sset.
		
		SET currentPitch TO pitchMin + ((pitchMax-pitchMin)/2).
		SET sset TO heading(90,currentPitch).
		UNTIL altitude > targetaltitude {
		
			// calculations
			SET altitudedifference TO targetaltitude-altitude.
			SET timetoaltitude TO altitudedifference/verticalspeed.
			SET predictedairspeed TO airspeed+ship:sensors:acc:mag*timetoaltitude.
			
			// pitch adjustment in the last 30s

			LOCAL possibleNewPitch TO 0.
			LOCAL newPitch TO 0.
			
			IF (predictedairspeed<targetspeed AND timetoaltitude<pitchtime) {
				// 0..1 value that will control how much to pitch, a guess
				SET urgency TO (pitchtime-timetoaltitude)/pitchtime.
				SET mag TO (targetspeed-predictedairspeed)*urgency.
				SET newPitch TO pitchMax - (mag*pitchsensitivity).
				IF (newPitch<pitchMin) {
					SET newPitch TO pitchMin.
				}
				SET possibleNewPitch TO newPitch.
			} else {
				SET possibleNewPitch TO pitchMax.
			}
			IF possibleNewPitch < currentPitch AND possibleNewPitch < currentPitch - maxPitchChange {
				SET newPitch TO currentPitch - maxPitchChange.
			} ELSE IF possibleNewPitch > currentPitch AND possibleNewPitch > currentPitch + maxPitchChange {
				SET newPitch TO currentPitch + maxPitchChange.
			} ELSE {
				SET newPitch TO possibleNewPitch.
			}
			SET currentPitch TO newPitch.
			SET sset TO heading(90,newPitch).
		
			lex["draw"]().
		}
		
		UNLOCK steering.
		UNLOCK throttle.
		
		lex["next"]().
	}).
	return lex.
}

FUNCTION CreateRocket {
	PARAMETER targetaltitude IS 75000.
	PARAMETER pitchUp IS 30.
	
	LOCAL sset TO heading(0,0).
	
	LOCAL lex TO CreateBase("rocket").
	
	lex:add("screen", {
		LOCAL screen TO lexicon().
		screen:add("name","Rocket Burn").
		screen:add("fields",list("Altitude Target")).
		screen:add("Altitude Target",targetaltitude).
		return screen.
	}).
	
	lex:add("main",{
	
		LOCK throttle TO 1.0.
		LOCK steering TO sset.
	
		TOGGLE AG2.
		TOGGLE AG3.
		
		SET sset TO heading(90,pitchUp).
		UNTIL apoapsis > targetaltitude { 
			lex["draw"]().
			SET sset TO heading(90,pitchUp).
		}
		
		UNLOCK throttle.
		UNLOCK steering.
		
		lex["next"]().
	}).
	return lex.
}

FUNCTION CreateCoast {
	LOCAL lex TO CreateBase("coast").
	
	lex:add("screen", {
		LOCAL screen TO lexicon().
		screen:add("name","Coast").
		screen:add("fields",list("Atmosphere Height")).
		screen:add("Atmosphere Height",body:atm:height).
		return screen.
	}).
	
	lex:add("main",{
		LOCK steering TO prograde.
		LOCK throttle TO 0.
		
		UNTIL altitude > body:atm:height { 
			lex["draw"]().
		}
		TOGGLE AG10.
		
		UNLOCK steering.
		UNLOCK throttle.
		
		lex["next"]().
	}).
	return lex.
}

FUNCTION CreateCircularize {
	LOCAL lex TO CreateBase("circularize").
	
	lex:add("main",{	
		set r_a to ship:apoapsis + ship:body:radius.
		set r_p to ship:periapsis + ship:body:radius.

		set ecc to 1 - (2 / ((r_a / r_p) + 1)).
		set sma to (r_p + r_a) / 2.

		set v_a to sqrt(((1 - ecc) * body:mu) / ((1 + ecc) * sma)).
		set v_p to sqrt(((1 + ecc) * body:mu) / ((1 - ecc) * sma)).

		set circDV to sqrt(body:mu / r_a) - v_a.
		add node(time:seconds + eta:apoapsis, 0, 0, circDV).
	
		lex["next"]().
	}).
	return lex.
}

FUNCTION ExecuteNode {
	LOCAL lex TO CreateBase("execute").
	
	LOCAL timeToBurn TO 0.
	LOCAL nodeIn TO 0.
	LOCAL deltaV TO 0.
	LOCAL maxAcceleration TO 0.
	LOCAL burnDuration TO 0.
	LOCAL executionStatus TO "".
	LOCAL timeToBurn TO 0.
	
	lex:add("screen", {
		LOCAL screen TO lexicon().
		screen:add("name","Circularize").
		screen:add("fields",list("Node In","Delta V","",
			"Max Acceleration","Burn Estimation","",
			"Status","",
			"Time To Burn")).
		
		screen:add("Node In",nodeIn).
		screen:add("Delta V",deltaV).
		screen:add("Max Acceleration",maxAcceleration).
		screen:add("Burn Estimation",burnDuration).
		screen:add("Status",executionStatus).
		screen:add("Time To Burn",timeToBurn).
				
		return screen.
	}).
	
	lex:add("main",{	
		
		lock throttle to 0.
		sas off.

		// grab our next node
		local mNode to nextnode.

		// figure out some information about it
		set burnDV to mNode:deltav.
		set burnDVMag to mNode:deltav:mag.
		set burnAcceleration to ship:maxthrust / ship:mass.
		set burnTime to burnDVMag / burnAcceleration.

		set executionStatus TO "Aligning".
		lock steering to mNode:deltav.
		until vang(ship:facing:forevector, mNode:deltav) < 1 {
			lex["draw"]().
		}

		set executionStatus TO "Warping".
		if(mNode:eta > 10) {
		  warpto(time:seconds + mNode:eta - (burnTime / 2) - 10). 
		}

		set executionStatus TO "Realigning".
		wait until (vang(ship:facing:forevector, mNode:deltav) < 1) or (mNode:eta <= (burnTime / 2)).
		warpto(time:seconds + mNode:eta - (burnTime / 2)).
		until mNode:eta <= (burnTime / 2) {
		  lex["draw"]().
		  wait 0.1.
		}

		set executionStatus TO "Burning".
		lock throttle to 1.

		until burnTime <= 2 {
		  set burnAcceleration to ship:maxthrust / ship:mass.
		  if burnAcceleration > 0.001 {
			set burnTime to mNode:deltav:mag / burnAcceleration.
		  }
		  lex["draw"]().
		  wait 0.1.
		}

		set executionStatus TO "Final Burn".
		until false {
			lex["draw"]().

			if(vdot(burnDV, mNode:deltav) < 0) {
				lock throttle to 0.
				break.
			}

			set burnAcceleration to ship:maxthrust / ship:mass.
			if(burnAcceleration > 0.001) {
				lock throttle to min(mNode:deltav:mag / burnAcceleration, 1).
			}
			else {
				lock throttle to 1.
			}

			// if we're close, hold tight
			if(mNode:deltav:mag <= 0.1) {
				set executionStatus TO "Finishing".
				until vdot(burnDV, mNode:deltav) < 0.5 {
					lex["draw"]().
				}
				break.
			}
			wait 0.01.
		}
		set ship:control:pilotmainthrottle to 0.
		lock throttle to 0.
		unlock steering.
		unlock throttle.
		
		lex["next"]().
	}).
	return lex.
}

FUNCTION CreateWaitForAngle {
	PARAMETER targetAngleStart TO -33.
	PARAMETER targetAngleEnd TO -32.
	
	LOCAL lex TO CreateBase("waiting").
	
	LOCAL vecA TO V(0,0,0).
	LOCAL vecB TO V(0,0,0).
	LOCAL ang TO vang(vecA,vecB).
	LOCAL cross TO vcrs(vecA,vecB).
	LOCAL estimate TO 0.
	LOCAL remainingAngle TO 0.
	
	lex:add("screen", {
		LOCAL screen TO lexicon().
		screen:add("name","Wait For Angle").
		screen:add("fields",list("Angle","Target","","Estimate","Remaining Angle","",
			"Ground Speed","Rate")).
		screen:add("Angle",ang).
		screen:add("Target",targetAngleStart).
		screen:add("Estimate",estimate).
		screen:add("Remaining Angle",remainingAngle).
		screen:add("Ground Speed",groundspeed).
		screen:add("Rate",kuniverse:timewarp:rate).

		return screen.
	}).
	
	lex:add("main",{
	
		function updateValues {
			SET vecA TO ship:position-SHIP:BODY:POSITION.
			SET vecB TO target:position-SHIP:BODY:POSITION.
			SET ang TO vang(vecA,vecB).
			SET cross TO vcrs(vecA,vecB).
			
			if(cross:y>0) {
				set ang to -1*ang.
			}
			
			if(ang>0) {
				set remainingAngle to (180-ang)+(180+targetAngleStart).
			} else if(ang<0) {
				set remainingAngle to abs(ang-targetAngleStart).
			}
			set estimate to target:obt:period * (remainingAngle/360).
		}
			
		set done to false.
		until done {
			updateValues().
			
			if(estimate>10) {
				until kuniverse:timewarp:mode="RAILS".
				warpto(time:seconds+(estimate-10)).
			} else
			if(ang>targetAngleStart and ang<targetAngleEnd) {
				set done to true.
			}
			lex["draw"]().
		}	
	
		lex["next"]().
	}).
	return lex.
}

SET SHIP:CONTROL:PILOTMAINTHROTTLE TO 0.

SET angle TO CreateWaitForAngle().
SET countdown TO CreateCountDown(2).
SET start TO CreateStart().
SET liftoff TO CreateLiftOff().
SET rocket TO CreateRocket(80000).
SET cruise TO CreateCruise().
SET coast TO CreateCoast().
SET circularize TO CreateCircularize().
SET execute TO ExecuteNode().

angle:add("after",countdown).
countdown:add("after",start).
start:add("after",liftoff).
liftoff:add("after",cruise).
cruise:add("after",rocket).
rocket:add("after",coast).
coast:add("after",circularize).
circularize:add("after",execute).

SET target TO Vessel("KSS2").

angle["run"]().

//CLEARSCREEN.
//LOCK throttle TO 0.
SET SHIP:CONTROL:PILOTMAINTHROTTLE TO 0.
