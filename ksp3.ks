run util.ks.
run orbit.ks.
Clearscreen.

stage.
Escape(targetAltitude,3).
CoastTo(40000).
FineTuneApoapsis(targetAltitude,0.1).

print "Waiting for Circularize...".
until eta:apoapsis <  10.
print "Circularize...".
LOCK SemiPeri TO max(ETA:PERIAPSIS,ETA:APOAPSIS)-min(ETA:PERIAPSIS,ETA:APOAPSIS).
LOCK PeriOff TO (180/SemiPeri) * ETA:APOAPSIS.
SET ExtraOffset TO 0.
SET accuracy TO 100.
LOCK STEERING TO HEADING(90,ExtraOffset-PeriOff) + R(0,0,0).
LOCK THROTTLE TO min(((ship:apoapsis-(ship:periapsis-accuracy))/20000),1).
WAIT UNTIL ship:periapsis > ship:apoapsis-accuracy.
LOCK THROTTLE TO 0.