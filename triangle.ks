function solveSASTriangle {
	parameter b.
	parameter c.
	parameter angleA.
	set triangle to createSASTriangle(b,c,angleA).
	set a to LawOfCosines(b,c,angleA).
	triangle:add("a",a).
	if b<c {
		set angleB to arcsin((sin(angleA)/a)*b).
		triangle:add("angleB",angleB).
		triangle:add("angleC",180-angleA-angleB).
	} else {
		set angleC to arcsin((sin(angleA)/a)*c).
		triangle:add("angleC",angleC).
		triangle:add("angleB",180-angleA-angleC).
	}
	return triangle.
}

function createSASTriangle {
	parameter b.
	parameter c.
	parameter angleA.
	set triangle to lexicon().
	triangle:add("b",b).
	triangle:add("c",c).
	triangle:add("angleA",angleA).
	return triangle.
}

function LawOfCosines {
	parameter b.
	parameter c.
	parameter angleA.
	return sqrt(b*b + c*c - 2 * b * c * COS(angleA)).
}

