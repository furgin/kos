


function Launch {
    CLEARSCREEN.
    PARAMETER count is 3.
    PRINT "Launch:".
    FROM {local countdown is count.}
    UNTIL countdown = 0
    STEP {SET countdown to countdown - 1.}
    DO {
        PRINT "..." + countdown.
        WAIT 1.
    }
}.

function Fly {
    CLEARSCREEN.
    LOCK THROTTLE TO 1.0.
    STAGE.
    LOCK STEERING TO UP.
    UNTIL ship:SOLIDFUEL = 0 {
        PRINT "Solid Fuel: "+ ROUND(ship:SOLIDFUEL,2) AT (0,0).
    }
}

Launch(3).
Fly().


