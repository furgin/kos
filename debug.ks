function DumpModules {
	for p in ship:parts {
		print p:name.
		for m in p:modules {
			print " "+m.
		}
	}
}

function WaitForAllAntenna {
	for m in ship:modulesnamed("ModuleDataTransmitter") {
		print "antenna [ "+m:name+ " ] from [ "+m:part:name+" ] is [ "+m:getfield("antenna state") +" ]".
		wait until m:getfield("antenna state") = "Idle".
	}
}

//DumpModules().
WaitForAllAntenna().