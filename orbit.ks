run util.ks.
run triangle.ks.

function Launch {
    PARAMETER count is 3.
    PRINT "Launch:" AT (0,0).
    FROM {local countdown is count.}
    UNTIL countdown = 0
    STEP {SET countdown to countdown - 1.}
    DO {
        PRINT "Launch: " + countdown AT (0,0).
        WAIT 1.
    }
    PRINT "".
}.

function AngleForAltitude {
    parameter shipAltitude.
    parameter shipTargetAltitude.
    parameter shipTargetPitch.
    parameter u to 1.
    parameter v to 1.

    set b to shipAltitude*u.
    set c to shipTargetAltitude-shipAltitude*v.

    local triangle to solveSASTriangle(b,c,shipTargetPitch).
    local angleA to triangle["angleA"].
    local angleB to triangle["angleB"].
    local angleC to triangle["angleC"].

    return angleB.
}

function Escape {
    parameter targetAltitude is 70000.
    set targetPitch to 90.
    parameter u to 1.

    local targetAltitude to 72000.
    lock throttle to 1.

    set pitch to 0.
    lock STEERING TO Up + R(0,-pitch,0).
    UNTIL ship:orbit:apoapsis > targetAltitude {       
        local pitchAngle to AngleForAltitude(ship:altitude,targetAltitude,targetPitch,u).
        lock STEERING TO Up + R(0,-pitchAngle,0).
        set screen to lexicon().
        screen:add("fields",list("Mode","-","Status","","Altitude","Apoapsis","Pitch")).
        screen:add("Mode",RightAlign("Escape",15)).
        screen:add("Status",RightAlign(ship:Status,15)).
        screen:add("Altitude",Number(ship:altitude,2,15)+"m").
        screen:add("Apoapsis",Number(ship:apoapsis,2,15)+"m").
        screen:add("Pitch",Number(pitchAngle,2,15)+"°").
        DrawScreen(screen).
    }
}

function CoastTo {
    parameter targetAltitude is 40000.
    lock throttle to 0.
    lock steering to ship:prograde.
    until ship:altitude > 40000 {
        set screen to lexicon().
        screen:add("fields",list("Mode","-","Altitude","Apoapsis")).
        screen:add("Mode",RightAlign("Coasting",15)).
        screen:add("Target",Number(targetAltitude,2,15)+"m").
        screen:add("Altitude",Number(ship:altitude,2,15)+"m").
        screen:add("Apoapsis",Number(ship:apoapsis,2,15)+"m").
        DrawScreen(screen).
    }
}

function FineTuneApoapsis {
    parameter targetApoapsis is 75000.

    lock throttle to 1.
    lock steering to ship:prograde.
    until ship:apoapsis > targetApoapsis {

        if(targetApoapsis-ship:apoapsis<5000) {
            lock throttle to 0.5.
        } else if(targetApoapsis-ship:apoapsis<2500) {
            lock throttle to 0.2.
        } else if(targetApoapsis-ship:apoapsis<1000) {
            lock throttle to 0.1.
        }

        set screen to lexicon().
        screen:add("fields",list("Mode","-","Altitude","Apoapsis")).
        screen:add("Mode",RightAlign("Coasting",15)).
        screen:add("Altitude",Number(ship:altitude,2,15)+"m").
        screen:add("Apoapsis",Number(ship:apoapsis,2,15)+"m").     
        screen:add("Throttle",Number(throttle,2,15)+"%").
        DrawScreen(screen).
    }
    lock throttle to 0.
}

function CircularizeAtApoapsis {
    parameter accuracy to 100.
    parameter throttleDownPeriod to 20000.

    until eta:apoapsis <  10 {
        set screen to lexicon().
        screen:add("fields",list("Mode","-","Apoapsis ETA")).
        screen:add("Mode",RightAlign("Waiting",15)).
        screen:add("Apoapsis ETA",RightAlign(eta:apoapsis,15)+"s").
        DrawScreen(screen). 
    }

    LOCK SemiPeri TO max(ETA:PERIAPSIS,ETA:APOAPSIS)-min(ETA:PERIAPSIS,ETA:APOAPSIS).
    LOCK PeriOff TO (180/SemiPeri) * ETA:APOAPSIS.
    SET ExtraOffset TO 0.
    
    LOCK STEERING TO HEADING(90,ExtraOffset-PeriOff) + R(0,0,0).
    LOCK THROTTLE TO min(((ship:apoapsis-(ship:periapsis-accuracy))/throttleDownPeriod),1).
    UNTIL ship:periapsis > ship:apoapsis-accuracy {
        set screen to lexicon().
        screen:add("fields",list("Mode","-","Apoapsis ETA","Apoapsis","Periapsis")).
        screen:add("Mode",RightAlign("Circularizing",15)).
        screen:add("Apoapsis ETA",RightAlign(eta:apoapsis,15)+"s").
        screen:add("Apoapsis",RightAlign(ship:orbit:apoapsis,1)+"m").
        screen:add("Periapsis",RightAlign(ship:orbit:periapsis,15)+"m").
        DrawScreen(screen). 
    }
    LOCK THROTTLE TO 0.
}

