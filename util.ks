
function Number {
    PARAMETER val.
    PARAMETER round.
    PARAMETER length.
    set s to ""+round(val,round).
    set r to s:find(".")-(s:length-3).
    if r>0 {
        set s to s+Repeat(r,"0").
    } else if r<0 {
        set s to s+".00".
    }
    return RightAlign(s,length).
}

function RightAlign {
    PARAMETER v.
    PARAMETER length.
    set s to ""+v.
    IF(length<s:length) {
        return s.
    }
    return Repeat(length-s:length," ")+s.
}

function Repeat {
    PARAMETER length.
    PARAMETER s.
    
    SET str TO "".
    
    FROM { local i is 0. }
    UNTIL i = length
    STEP { SET i to i+1. }
    DO { SET str TO str+s. }
    
    RETURN str.
}

FUNCTION Underline {
    PARAMETER length.
    RETURN Repeat(length,"-").
}

FUNCTION DrawScreen {
    PARAMETER screen.
    PARAMETER x IS 0.
    PARAMETER y IS 0.
    PARAMETER labelLength IS 20.
    PARAMETER width IS Terminal:Width.

    // Get Fields
    FROM {
        local i is 0.
        local itr is screen["fields"]:iterator.
    }
    UNTIL not itr:next
    STEP { SET i to i+1. }
    DO {
        if (itr:value = "-") {
            PRINT Underline(width) AT(x,y+i).
        } else if (itr:value = "") {
        } else if(not screen:haskey(itr:value)) {
            PRINT itr:value AT (x+1,y+i).
            PRINT "unknown" AT (x+labelLength+1,y+i).
        } else if(NOT (itr:value = "")) {
            PRINT itr:value AT (x+1,y+i).
            PRINT screen[itr:value] AT (x+labelLength+1,y+i).
        }
    }
}